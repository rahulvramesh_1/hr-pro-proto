<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends CI_Controller {

	public function index()
	{
	$this->load->view("layout/layout_main");

    $this->load->view("Clients/addnewclient");
	$this->load->view("layout/footer");
	}

  /*
  * Add New Client
  */
  public function create()
	{

	}

  /*
  * List All Clients (Use Sata Table From Theme)
  * Note : It Should Contain Buttons For Edit and Delete
  */
  public function list_all()
	{
	$this->load->view("layout/layout_main");
 $this->load->view("Clients/viewclient");
	$this->load->view("layout/footer");

	}
	 public function editDelete()
	{
	 $this->load->view("layout/layout_main");

 $this->load->view("Clients/editclient");
 $this->load->view("layout/footer");

	}

	 public function invoice()
	{
	 $this->load->view("layout/layout_main");

 $this->load->view("Clients/newinvoice");
 $this->load->view("layout/footer");

	}
 public function manageinvoice()
	{
	 $this->load->view("layout/layout_main");

 $this->load->view("Clients/manageinvoice");
 $this->load->view("layout/footer");

	}
	public function viewinvoice()
	{
	 $this->load->view("layout/layout_main");

 $this->load->view("Clients/invoice");
 $this->load->view("layout/footer");

	}

}
