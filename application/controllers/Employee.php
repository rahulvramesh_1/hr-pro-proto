<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	public function index()
	{

	}

  /*
  * Add New Employee
  */
  public function create()
	{
$this->load->view("layout/layout_main");
$this->load->view("employees/employee_new");
$this->load->view("layout/footer");
	}

  /*
  * List All Employees (Use Sata Table From Theme)
  * Note : It Should Contain Buttons For Edit and Delete
  */
  public function list_all()
	{
$this->load->view("layout/layout_main");
$this->load->view("employees/employee_view");
$this->load->view("layout/footer");
	}
public function editEmp()
  {
$this->load->view("layout/layout_main");
$this->load->view("employees/employee_edit");
$this->load->view("layout/footer");
  }
public function generate()
  {
$this->load->view("layout/layout_main");
$this->load->view("employees/Gen_payslip");
$this->load->view("layout/footer");
  }
public function pay()
  {
$this->load->view("layout/layout_main");
$this->load->view("employees/custom");
$this->load->view("layout/footer");
  }
public function dashview()
  {
$this->load->view("layout/layout_main");
$this->load->view("Assign/dashboard_view2");
$this->load->view("layout/footer");
  }
  public function lassign()
  {
$this->load->view("layout/layout_main");
$this->load->view("Assign/lassign");
$this->load->view("layout/footer");
  }
}
