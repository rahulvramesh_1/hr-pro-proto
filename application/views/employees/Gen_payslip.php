 <div class="wrapper">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <!-- <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button> -->
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <h4 class="page-title">Payslip</h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <!-- <div class="panel-heading">
                                <h4>Invoice</h4>
                            </div> -->
                            <div class="panel-body">
                                <div class="clearfix">
                                    <div class="pull-left">
                                        <h3 class="logo invoice-logo" align="center">Alindalpro</h3>
                                    </div>
                                    <div class="pull-right">
                                        <h4>Payslip # <br>
                                            <strong>2016-10-00001</strong>
                                        </h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="pull-left m-t-30">
                                            <address>
                                              <strong>Employee Info</strong><br>
                                              Address line 1<br>
                                              Address line 2<br>
                                              <abbr title="Phone">P:</abbr> (123) 456-7890
                                              </address>
                                        </div>
                                        <div class="pull-right m-t-30">
                                            <p><strong>Payment Period From: </strong> Jan 17, 2016</p>
                                            <p class="m-t-10"><strong>Payment Period To: </strong> Feb 17, 2016</p>
                                            <p class="m-t-10"><strong>Employee ID: </strong> #123456</p>
                                        </div>
                                    </div><!-- end col -->
                                </div>
                                <!-- end row -->

                                <div class="m-h-50"></div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table m-t-30">
                                                <thead>
                                                    <tr><th>#</th>
                                                    <th>Particular</th>
                                                    <th>Description</th>
                                                    <th>Days</th>
                                                    <th>Per day</th>
                                                    <th>Total</th>
                                                </tr></thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Travelling</td>
                                                        <td>Lorem ipsum dolor sit amet.</td>
                                                        <td>1</td>
                                                        <td>AED380</td>
                                                        <td>AED380</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Food</td>
                                                        <td>Lorem ipsum dolor sit amet.</td>
                                                        <td>5</td>
                                                        <td>AED50</td>
                                                        <td>AED250</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Others</td>
                                                        <td>Lorem ipsum dolor sit amet.</td>
                                                        <td>2</td>
                                                        <td>AED500</td>
                                                        <td>AED1000</td>
                                                    </tr>
                                                   
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <div class="clearfix m-t-40">
                                            <h5 class="small text-inverse font-600">PAYMENT TERMS AND POLICIES</h5>

                                            <small>
                                                Terms and conditions&Terms and conditionsTerms and conditions
                                                Terms and conditionsTerms and conditionsTerms and conditions
                                                Terms and conditionsTerms and conditionsTerms and conditions
                                               Terms and conditionsTerms and conditionsTerms and conditions
                                                Terms and conditionsTerms and conditions
                                            </small>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-6 col-md-offset-3">
                                        <p class="text-right"><b>Sub-total:</b> 2930.00</p>
                                        <p class="text-right">Discout: 12.9%</p>
                                        <p class="text-right">VAT: 12.9%</p>
                                        <hr>
                                        <h3 class="text-right">AED 100.00</h3>
                                    </div>
                                </div>
                                <hr>
                                <div class="hidden-print">
                                    <div class="pull-right">
                                        <a href="javascript:window.print()" class="btn btn-inverse waves-effect waves-light"><i class="fa fa-print"></i></a>
                                        <a href="#" class="btn btn-primary waves-effect waves-light">Generate Payslip</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div><!-- <div class="row">
                  <div class="col-lg-4">
                      <div class="card-box">
                          <div class="dropdown pull-right">
                              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                 aria-expanded="false">
                                  <i class="zmdi zmdi-more-vert"></i>
                              </a>
                              <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                              </ul>
                          </div> -->

                         <!--  <h4 class="header-title m-t-0">Daily Sales</h4>

                          <div class="widget-chart text-center">
                              <div id="morris-donut-example" style="height: 245px;"></div>
                              <ul class="list-inline chart-detail-list m-b-0">
                                  <li>
                                      <h5 style="color: #ff8acc;"><i class="fa fa-circle m-r-5"></i>Series A</h5>
                                  </li>
                                  <li>
                                      <h5 style="color: #5b69bc;"><i class="fa fa-circle m-r-5"></i>Series B</h5>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div><! end col --> 

                  <!-- <div class="col-lg-4">
                      <div class="card-box">
                          <div class="dropdown pull-right">
                              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                 aria-expanded="false">
                                  <i class="zmdi zmdi-more-vert"></i>
                              </a>
                              <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                              </ul>
                          </div>
                          <h4 class="header-title m-t-0">Statistics</h4>
                          <div id="morris-bar-example" style="height: 280px;"></div>
                      </div>
                  </div> --><!-- end col -->

                  <!-- <div class="col-lg-4">
                      <div class="card-box">
                          <div class="dropdown pull-right">
                              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                 aria-expanded="false">
                                  <i class="zmdi zmdi-more-vert"></i>
                              </a>
                              <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                              </ul>
                          </div>
                          <h4 class="header-title m-t-0">Total Revenue</h4>
                          <div id="morris-line-example" style="height: 280px;"></div>
                      </div>
                  </div> --><!-- end col -->

              <!-- </div> -->
              <!-- end row -->


              <!-- <div class="row">
                  <div class="col-lg-3 col-md-6">
                      <div class="card-box widget-user">
                          <div>
                              <img src="<?php echo base_url(); ?>assets/images/users/avatar-3.jpg" class="img-responsive img-circle"
                                   alt="user">
                              <div class="wid-u-info">
                                  <h4 class="m-t-0 m-b-5 font-600">Chadengle</h4>
                                  <p class="text-muted m-b-5 font-13">coderthemes@gmail.com</p>
                                  <small class="text-warning"><b>Admin</b></small>
                              </div>
                          </div>
                      </div>
                  </div> --><!-- end col -->

                  <!-- <div class="col-lg-3 col-md-6">
                      <div class="card-box widget-user">
                          <div>
                              <img src="<?php echo base_url(); ?>assets/images/users/avatar-2.jpg" class="img-responsive img-circle"
                                   alt="user">
                              <div class="wid-u-info">
                                  <h4 class="m-t-0 m-b-5 font-600"> Michael Zenaty</h4>
                                  <p class="text-muted m-b-5 font-13">coderthemes@gmail.com</p>
                                  <small class="text-custom"><b>Support Lead</b></small>
                              </div>
                          </div>
                      </div>
                  </div> --><!-- end col -->

                  <!-- <div class="col-lg-3 col-md-6">
                      <div class="card-box widget-user">
                          <div>
                              <img src="<?php echo base_url(); ?>assets/images/users/avatar-1.jpg" class="img-responsive img-circle"
                                   alt="user">
                              <div class="wid-u-info">
                                  <h4 class="m-t-0 m-b-5 font-600">Stillnotdavid</h4>
                                  <p class="text-muted m-b-5 font-13">coderthemes@gmail.com</p>
                                  <small class="text-success"><b>Designer</b></small>
                              </div>
                          </div>
                      </div>
                  </div> --><!-- end col -->

                  <!-- <div class="col-lg-3 col-md-6">
                      <div class="card-box widget-user">
                          <div>
                              <img src="<?php echo base_url(); ?>assets/images/users/avatar-10.jpg" class="img-responsive img-circle"
                                   alt="user">
                              <div class="wid-u-info">
                                  <h4 class="m-t-0 m-b-5 font-600">Tomaslau</h4>
                                  <p class="text-muted m-b-5 font-13">coderthemes@gmail.com</p>
                                  <small class="text-info"><b>Developer</b></small>
                              </div>
                          </div>
                      </div>
                  </div> --><!-- end col -->
              <!-- </div> -->
              <!-- end row -->


              <!-- <div class="row">
                  <div class="col-lg-4">
                      <div class="card-box">
                          <div class="dropdown pull-right">
                              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                 aria-expanded="false">
                                  <i class="zmdi zmdi-more-vert"></i>
                              </a>
                              <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                              </ul>
                          </div> -->

                         <!--  <h4 class="header-title m-t-0 m-b-30">Inbox</h4>

                          <div class="inbox-widget nicescroll" style="height: 315px;">
                              <a href="#">
                                  <div class="inbox-item">
                                      <div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/users/avatar-1.jpg"
                                                                       class="img-circle" alt=""></div>
                                      <p class="inbox-item-author">Chadengle</p>
                                      <p class="inbox-item-text">Hey! there I'm available...</p>
                                      <p class="inbox-item-date">13:40 PM</p>
                                  </div> -->
                              <!-- </a>
                              <a href="#">
                                  <div class="inbox-item">
                                      <div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/users/avatar-2.jpg"
                                                                       class="img-circle" alt=""></div>
                                      <p class="inbox-item-author">Tomaslau</p>
                                      <p class="inbox-item-text">I've finished it! See you so...</p>
                                      <p class="inbox-item-date">13:34 PM</p>
                                  </div>
                              </a>
                              <a href="#">
                                  <div class="inbox-item">
                                      <div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/users/avatar-3.jpg"
                                                                       class="img-circle" alt=""></div>
                                      <p class="inbox-item-author">Stillnotdavid</p>
                                      <p class="inbox-item-text">This theme is awesome!</p>
                                      <p class="inbox-item-date">13:17 PM</p>
                                  </div>
                              </a>
                              <a href="#">
                                  <div class="inbox-item">
                                      <div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/users/avatar-4.jpg"
                                                                       class="img-circle" alt=""></div>
                                      <p class="inbox-item-author">Kurafire</p>
                                      <p class="inbox-item-text">Nice to meet you</p>
                                      <p class="inbox-item-date">12:20 PM</p>
                                  </div>
                              </a>
                              <a href="#">
                                  <div class="inbox-item">
                                      <div class="inbox-item-img"><img src="<?php echo base_url(); ?>assets/images/users/avatar-5.jpg"
                                                                       class="img-circle" alt=""></div>
                                      <p class="inbox-item-author">Shahedk</p>
                                      <p class="inbox-item-text">Hey! there I'm available...</p>
                                      <p class="inbox-item-date">10:15 AM</p> -->
                                <!--   </div>
                              </a>
                          </div>
                      </div>
                  </div> --><!-- end col -->

                 <!--  <div class="col-lg-8">
                      <div class="card-box">
                          <div class="dropdown pull-right">
                              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown"
                                 aria-expanded="false">
                                  <i class="zmdi zmdi-more-vert"></i>
                              </a>
                              <ul class="dropdown-menu" role="menu">
                                  <li><a href="#">Action</a></li>
                                  <li><a href="#">Another action</a></li>
                                  <li><a href="#">Something else here</a></li>
                                  <li class="divider"></li>
                                  <li><a href="#">Separated link</a></li>
                              </ul>
                          </div>

                          <h4 class="header-title m-t-0 m-b-30">Latest Projects</h4>

                          <div class="table-responsive">
                              <table class="table">
                                  <thead>
                                  <tr>
                                      <th>#</th>
                                      <th>Project Name</th>
                                      <th>Start Date</th>
                                      <th>Due Date</th>
                                      <th>Status</th>
                                      <th>Assign</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  <tr>
                                      <td>1</td>
                                      <td>Adminto Admin v1</td>
                                      <td>01/01/2016</td>
                                      <td>26/04/2016</td>
                                      <td><span class="label label-danger">Released</span></td>
                                      <td>Coderthemes</td>
                                  </tr>
                                  <tr>
                                      <td>2</td>
                                      <td>Adminto Frontend v1</td>
                                      <td>01/01/2016</td>
                                      <td>26/04/2016</td>
                                      <td><span class="label label-success">Released</span></td>
                                      <td>Adminto admin</td>
                                  </tr>
                                  <tr>
                                      <td>3</td>
                                      <td>Adminto Admin v1.1</td>
                                      <td>01/05/2016</td>
                                      <td>10/05/2016</td>
                                      <td><span class="label label-pink">Pending</span></td>
                                      <td>Coderthemes</td>
                                  </tr>
                                  <tr>
                                      <td>4</td>
                                      <td>Adminto Frontend v1.1</td>
                                      <td>01/01/2016</td>
                                      <td>31/05/2016</td>
                                      <td><span class="label label-purple">Work in Progress</span>
                                      </td>
                                      <td>Adminto admin</td>
                                  </tr>
                                  <tr>
                                      <td>5</td>
                                      <td>Adminto Admin v1.3</td>
                                      <td>01/01/2016</td>
                                      <td>31/05/2016</td>
                                      <td><span class="label label-warning">Coming soon</span></td>
                                      <td>Coderthemes</td>
                                  </tr>

                                  <tr>
                                      <td>6</td>
                                      <td>Adminto Admin v1.3</td>
                                      <td>01/01/2016</td>
                                      <td>31/05/2016</td>
                                      <td><span class="label label-primary">Coming soon</span></td>
                                      <td>Adminto admin</td>
                                  </tr>

                                  <tr>
                                      <td>7</td>
                                      <td>Adminto Admin v1.3</td>
                                      <td>01/01/2016</td>
                                      <td>31/05/2016</td>
                                      <td><span class="label label-primary">Coming soon</span></td>
                                      <td>Adminto admin</td>
                                  </tr>

                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div> --><!-- end col -->

            <!--   </div> -->
              <!-- end row -->


              <!-- Footer -->
              <footer class="footer text-right">
                  <div class="container">
                      <div class="row">
                          <div class="col-xs-6">
                              2016 © Alindal Technologies.
                          </div>
                          <div class="col-xs-6">
                              <ul class="pull-right list-inline m-b-0">
                                  <li>
                                      <a href="#">About</a>
                                  </li>
                                  <li>
                                      <a href="#">Help</a>
                                  </li>
                                  <li>
                                      <a href="#">Contact</a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </footer>
              <!-- End Footer -->

          </div>
          <!-- end container -->



          <!-- Right Sidebar -->
          <div class="side-bar right-bar">
              <a href="javascript:void(0);" class="right-bar-toggle">
                  <i class="zmdi zmdi-close-circle-o"></i>
              </a>
              <h4 class="">Notifications</h4>
              <div class="notification-list nicescroll">
                  <ul class="list-group list-no-border user-list">
                      <li class="list-group-item">
                          <a href="#" class="user-list-item">
                              <div class="avatar">
                                  <img src="<?php echo base_url(); ?>assets/images/users/avatar-2.jpg" alt="">
                              </div>
                              <div class="user-desc">
                                  <span class="name">Michael Zenaty</span>
                                  <span class="desc">There are new settings available</span>
                                  <span class="time">2 hours ago</span>
                              </div>
                          </a>
                      </li>
                      <li class="list-group-item">
                          <a href="#" class="user-list-item">
                              <div class="icon bg-info">
                                  <i class="zmdi zmdi-account"></i>
                              </div>
                              <div class="user-desc">
                                  <span class="name">New Signup</span>
                                  <span class="desc">There are new settings available</span>
                                  <span class="time">5 hours ago</span>
                              </div>
                          </a>
                      </li>
                      <li class="list-group-item">
                          <a href="#" class="user-list-item">
                              <div class="icon bg-pink">
                                  <i class="zmdi zmdi-comment"></i>
                              </div>
                              <div class="user-desc">
                                  <span class="name">New Message received</span>
                                  <span class="desc">There are new settings available</span>
                                  <span class="time">1 day ago</span>
                              </div>
                          </a>
                      </li>
                      <li class="list-group-item active">
                          <a href="#" class="user-list-item">
                              <div class="avatar">
                                  <img src="<?php echo base_url(); ?>assets/images/users/avatar-3.jpg" alt="">
                              </div>
                              <div class="user-desc">
                                  <span class="name">James Anderson</span>
                                  <span class="desc">There are new settings available</span>
                                  <span class="time">2 days ago</span>
                              </div>
                          </a>
                      </li>
                      <li class="list-group-item active">
                          <a href="#" class="user-list-item">
                              <div class="icon bg-warning">
                                  <i class="zmdi zmdi-settings"></i>
                              </div>
                              <div class="user-desc">
                                  <span class="name">Settings</span>
                                  <span class="desc">There are new settings available</span>
                                  <span class="time">1 day ago</span>
                              </div>
                          </a>
                      </li>

                  </ul>
              </div>
          </div>
