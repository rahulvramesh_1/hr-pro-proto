


      <div class="wrapper">
          <div class="container">

            <?php //echo $content_for_layout;?>
 <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <h4 class="page-title">Add New Client </h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-6">
                        <div class="card-box">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Personal Informations</h4>

                            <form action="#" data-parsley-validate novalidate>
                                <div class="form-group">
                                    <label for="userName">First Name *</label>
                                    <input type="text" name="nick" parsley-trigger="change" required
                                           placeholder="First Name" class="form-control" id="userName">
                                </div>
								 <div class="form-group">
                                    <label for="userName">Last Name *</label>
                                    <input type="text" name="nick" parsley-trigger="change" required
                                           placeholder="Enter Last name" class="form-control" id="userName">
                                </div>
								 <div class="form-group">
                                    <label for="userName">Address*</label>
                                   
                                         
                                                <textarea required class="form-control"></textarea>
                                        
                                       
                                </div>
                                <div class="form-group">
                                    <label for="emailAddress">Email address*</label>
                                    <input type="email" name="email" parsley-trigger="change" required
                                           placeholder="Enter email" class="form-control" id="emailAddress">
                                </div>
								 <div class="form-group">
                                    <label for="userName">Contact No *</label>
                                    <input type="text" name="nick" parsley-trigger="change" required
                                           placeholder="Enter Contact No" class="form-control" id="userName">
                                </div>
                                <div class="form-group">
                                    <label for="pass1">Country *</label>
                                    <input id="pass1" type="password" placeholder="Country" required
                                           class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="passWord2">State *</label>
                                    <input data-parsley-equalto="#pass1" type="text" required
                                           placeholder="State" class="form-control" id="passWord2">
                                </div>
								 <div class="form-group">
                                    <label for="userName">City *</label>
                                    <input type="text" name="nick" parsley-trigger="change" required
                                           placeholder="City" class="form-control" id="userName">
                                </div>
                               

                                <div class="form-group text-right m-b-0">
                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                        Submit
                                    </button>
                                    <button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
                                        Cancel
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
          </div>
      </div>
 
