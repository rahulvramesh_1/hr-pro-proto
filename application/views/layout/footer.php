
      	<script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <?php echo script_tag('assets/js/jquery.min.js');?>
        <?php echo script_tag('assets/js/bootstrap.min.js');?>
        <?php echo script_tag('assets/js/detect.js');?>
        <?php echo script_tag('assets/js/fastclick.js');?>
        <?php echo script_tag('assets/js/jquery.slimscroll.js');?>
        <?php echo script_tag('assets/js/jquery.blockUI.js');?>
        <?php echo script_tag('assets/js/waves.js');?>
        <?php echo script_tag('assets/js/wow.min.js');?>
        <?php echo script_tag('assets/js/jquery.nicescroll.js');?>
        <?php echo script_tag('assets/js/jquery.scrollTo.min.js');?>

        <!-- App js -->
        <?php echo script_tag('assets/js/jquery.core.js');?>
        <?php echo script_tag('assets/js/jquery.app.js');?>
        <?php echo isset($script_links) ? $script_links : '' ?>

	</body>
</html>
